/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  riantsoa
 * Created: 10 juil. 2021
 */

create view voituremarque as
select
	v.id,
	v.matricule ,
	m.marque as idmarque
from
	voiture v
join marque m on
	v.idmarque = m.id ;
	
create view tokenValide as
select
	*
from
	token
where
	expiration >= now();