/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  riantsoa
 * Created: 10 juil. 2021
 */
/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/*==============================================================*/

/*
drop table COMMENTAIRE;

drop table MARQUE;

drop table TOKEN;

drop table UTILISATEUR;

drop table VOITURE;
*/
/*==============================================================*/
/* Table: COMMENTAIRE                                           */
/*==============================================================*/
create table COMMENTAIRE (
   ID                   VARCHAR(50)                  not null,
   IDUTILISATEUR        VARCHAR(50)                  not null,
   IDVOITURE            VARCHAR(50)                  not null,
   COMMENTAIRE          VARCHAR(200)         not null,
   constraint PK_COMMENTAIRE primary key (ID)
);

/*==============================================================*/
/* Table: MARQUE                                                */
/*==============================================================*/
create table MARQUE (
   ID                   VARCHAR(50)                  not null,
   MARQUE               VARCHAR(50)          not null,
   constraint PK_MARQUE primary key (ID)
);

/*==============================================================*/
/* Table: TOKEN                                                 */
/*==============================================================*/
create table TOKEN (
   IDUTILISATEUR        VARCHAR(50)                  not null,
   EXPIRATION           TIMESTAMP            not null,
   TOKEN                VARCHAR(200)         not null
);

/*==============================================================*/
/* Table: UTILISATEUR                                           */
/*==============================================================*/
create table UTILISATEUR (
   ID                   VARCHAR(50)                  not null,
   PSEUDO               VARCHAR(50)          not null,
   MDP                  VARCHAR(200)         not null,
   constraint PK_UTILISATEUR primary key (ID)
);

/*==============================================================*/
/* Table: VOITURE                                               */
/*==============================================================*/
create table VOITURE (
   ID                   VARCHAR(50)                  not null,
   MATRICULE            VARCHAR(50)          not null,
   IDMARQUE             VARCHAR(50)                  not null,
   constraint PK_VOITURE primary key (ID)
);

alter table COMMENTAIRE
   add constraint FK_COMMENTA_REFERENCE_UTILISAT foreign key (IDUTILISATEUR)
      references UTILISATEUR (ID)
      on delete restrict on update restrict;

alter table COMMENTAIRE
   add constraint FK_COMMENTA_REFERENCE_VOITURE foreign key (IDVOITURE)
      references VOITURE (ID)
      on delete restrict on update restrict;

alter table TOKEN
   add constraint FK_TOKEN_REFERENCE_UTILISAT foreign key (IDUTILISATEUR)
      references UTILISATEUR (ID)
      on delete restrict on update restrict;

alter table VOITURE
   add constraint FK_VOITURE_REFERENCE_MARQUE foreign key (IDMARQUE)
      references MARQUE (ID)
      on delete restrict on update restrict;


