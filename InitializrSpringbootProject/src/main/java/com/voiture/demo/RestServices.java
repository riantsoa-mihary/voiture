/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.voiture.demo;

import commentaire.Commentaire;
import commentaire.CommentaireDetail;
import commentaire.CommenterVoiture;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import voiture.ListeCommentaire;
import voiture.Voiture;

/**
 *
 * @author riantsoa
 */
@Controller
public class RestServices {
    @GetMapping(value = "/")
    public ResponseEntity<String> pong() 
    {
        return new ResponseEntity<String>("Réponse du serveur: "+HttpStatus.OK.name(), HttpStatus.OK);
    }
    @GetMapping(value = "/voiture/liste")
    public ResponseEntity<Object> listeVoiture() 
    {
        ResponseEntity response = null;
        Voiture[] liste = new Voiture[0];
        try {
            liste =  Voiture.getListe();
            response = new ResponseEntity<Object>(liste, HttpStatus.OK);
        } catch (Exception ex) {
            Logger.getLogger(RestServices.class.getName()).log(Level.SEVERE, null, ex);
            response = new ResponseEntity<Object>(ex.getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
        }
        return response;
    }

    @RequestMapping(value = "/commentaire/inserer", method = RequestMethod.POST)
    public ResponseEntity<Object> commenter(
            @RequestHeader("Authentification") String token, 
            @RequestParam("idVoiture") String idVoiture,
            @RequestParam("commentaire") String commentaire) 
    {
       
        ResponseEntity response = null;
        try {
            CommenterVoiture.doIt(token, idVoiture, commentaire);
            response = new ResponseEntity<Object>("success", HttpStatus.OK);
        } catch (Exception ex) {
            Logger.getLogger(RestServices.class.getName()).log(Level.SEVERE, null, ex);
            response = new ResponseEntity<Object>(ex.getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
        }
        return response;
    }
    @RequestMapping(value = "/commentaire/liste", method = RequestMethod.GET)
    public ResponseEntity<Object> listeCommentaire(
            @RequestHeader("Authentification") String token, 
            @RequestParam("idVoiture") String idVoiture)
    {
        ResponseEntity response = null;
        try {
            CommentaireDetail[] liste =  ListeCommentaire.get(token, idVoiture);
            response = new ResponseEntity<Object>(liste, HttpStatus.OK);
        } catch (Exception ex) {
            Logger.getLogger(RestServices.class.getName()).log(Level.SEVERE, null, ex);
            response = new ResponseEntity<Object>(ex.getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
        }
        return response;
    }

}
