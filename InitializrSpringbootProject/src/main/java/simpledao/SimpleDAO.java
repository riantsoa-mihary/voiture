/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpledao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author riantsoa
 */
public class SimpleDAO {

    
    public static void executeInsert(String sql, Connection connection)throws Exception{
        Statement statement = null;
        boolean isIntern = false;
        try {
            if(connection == null){
                connection = DB.getInstance();
                isIntern = true;
            }
            statement = connection.createStatement();
            statement.execute(sql);
        }catch(Exception e){
            throw e;
        }
        finally{
            if(statement != null)
                statement.close();
            if(connection != null && isIntern){
                connection.close();
            }
        }
    }
    public static void create(String[] champ, String table, Object mapping, Connection c) throws Exception{
        RequestInsert req = new RequestInsert(champ, table, mapping);
        executeInsert(req.getSQL(),c);
        
    }
    public static void create(String request, Connection c) throws Exception{
        executeInsert(request,c);
    }
    /*
    public static Object[] read(String[] champ, String table, Object mapping){
        RequestSelect req = new RequestSelect(champ, table, mapping);
        req.execute();
    }*/
    
}
