/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpledao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author riantsoa
 */
public class DB {
    public static Connection getInstance() throws Exception {
	Connection c = null;
	try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql:voiture","voiture","voiture");
	} catch (Exception e) {
            throw e;
	}
	return c; 
    }
}
