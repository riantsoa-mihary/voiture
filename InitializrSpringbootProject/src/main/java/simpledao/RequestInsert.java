/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpledao;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author riantsoa
 */
public class RequestInsert {

    private String table;
    private Object mapping;
        
    private String colonne;
    private String values;
    
    
    private void addColonne(String champ){
        if(this.colonne.equalsIgnoreCase("") == false){
            colonne += ",";
        }
        this.colonne += champ;
    }
    private void addValue(String champ) throws Exception{
        if(this.values.equalsIgnoreCase("") == false){
            values += ",";
        }
        Class classe = mapping.getClass();
        Field field = classe.getDeclaredField(champ);
        String type = field.getType().getName();
        Method method = classe.getMethod("get"+Utilitaire.upperFirst(champ), new Class[0]);
        String valeur = method.invoke(mapping, new Object[0]).toString();
        if(type.equalsIgnoreCase("java.lang.String") ||
                type.equalsIgnoreCase("java.sql.Date") ||
                type.equalsIgnoreCase("java.util.Date")){
            this.values += "'"+valeur+"'";
        }else{
            this.values += valeur;
        }
        
    }
    private void add(String champ) throws Exception{
        this.addColonne(champ);
        this.addValue(champ);
    }
    RequestInsert(String[] champ, String table, Object mapping) throws Exception {
        this.setTable(table);
        this.setMapping(mapping);
        this.setColonne("");
        this.setValue("");
        for(String c : champ){
            this.add(c);
        }
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public Object getMapping() {
        return mapping;
    }

    public void setMapping(Object mapping) {
        this.mapping = mapping;
        
    }

    public String getSQL(){
        String sql = " insert into "+table+" ";
        sql += "("+this.colonne+") values ";
        sql += "("+this.values+") ";
        return sql;
    }


    private void setColonne(String colonne) {
        this.colonne = colonne;
    }

    private void setValue(String values) {
        this.values = values;
    }
    
}
