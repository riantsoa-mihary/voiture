/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simpledao;

/**
 *
 * @author riantsoa
 */
public class Utilitaire {
    public static String upperFirst(String s){
        String up = "";
        char[] tab = s.toCharArray();
        up = String.valueOf(tab[0]);
        up = up.toUpperCase();
        for( int i = 1 ; i < tab.length ; i++){
            up += String.valueOf(tab[i]);
        }
        return up;
    }
}
