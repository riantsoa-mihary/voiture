/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilisateur;

import commentaire.Commentaire;
import commentaire.CommentaireDetail;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author riantsoa
 */
public class Utilisateur {
    private String id;

    public Utilisateur(String id) {
        this.setId(id);
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
}
