/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilisateur;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author riantsoa
 */
public class Token {

    private String token;

    public Token(String token) throws Exception {
        this.setToken(token);
    }
    public Utilisateur getUtilisateur(Connection c) throws Exception{

        Utilisateur u = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet =  null;
        try {
            String request = "select idUtilisateur from tokenValide "
                    + " where token = ? ";
            preparedStatement = c.prepareStatement(request);
            preparedStatement.setString(1, this.getToken());
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                u = new Utilisateur(resultSet.getString("idUtilisateur"));
                break;
            }
            if(u == null)
                throw new Exception("Authentification Token invalide");
        } catch (Exception ex) {
            throw ex;
        }finally{
            if(resultSet != null)
                resultSet.close();
            if(preparedStatement != null)
                preparedStatement.close();
        }
        return u;
    }
    public String getToken() {
        return token;
    }

    public void setToken(String token) throws Exception {
        if(token == null || token.equalsIgnoreCase(""))
            throw new Exception("Authentification Token requise");
        this.token = token;
    }
    
    
    
}
