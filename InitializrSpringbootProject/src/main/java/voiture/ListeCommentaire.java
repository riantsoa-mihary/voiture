/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package voiture;

import commentaire.Commentaire;
import commentaire.CommentaireDetail;
import java.sql.Connection;
import simpledao.DB;
import utilisateur.Token;

/**
 *
 * @author riantsoa
 */
public class ListeCommentaire {
    private Token token;
    private Voiture voiture;

    public ListeCommentaire(String token, String idVoiture) throws Exception {
        this.token = new Token(token);
        this.voiture = new Voiture(idVoiture);
    }
    public CommentaireDetail[] get() throws Exception{
        Connection c = null;
        CommentaireDetail[] liste = new CommentaireDetail[0];
        try {
            c = DB.getInstance();
            this.token.getUtilisateur(c);
            liste = CommentaireDetail.listeCommentaire(c, this.voiture.getId());
        } catch (Exception ex) {
            throw ex;
        }finally{
            if(c != null)
                c.close();
        }
        return liste;
    }
    public static CommentaireDetail[] get(String token, String idVoiture) throws Exception{
        ListeCommentaire lc = new ListeCommentaire(token, idVoiture);
        CommentaireDetail[] liste =  lc.get(); 
        return liste;
    }
    
}
