/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package voiture;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import simpledao.DB;

/**
 *
 * @author riantsoa
 */
public class Voiture {
    private String id;
    private String matricule;
    private String idMarque;
    public Voiture(String id) throws Exception{
        this.setId(id);
    }
    public Voiture(String id, String matricule, String idMarque) throws Exception {
        this.setId(id);
        this.setMatricule(matricule);
        this.setIdMarque(idMarque);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) throws Exception {
        if(id == null || id.equalsIgnoreCase(""))
            throw new Exception("id de la voiture requise");
        this.id = id;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getIdMarque() {
        return idMarque;
    }

    public void setIdMarque(String idMarque) {
        this.idMarque = idMarque;
    }
    /**
     * fonction pour prendre la liste des voitures
     * @return 
     */
    public static Voiture[] getListe() throws Exception{
        Connection c = null;
        Voiture[] voitures = null;
        Vector v = new Vector();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet =  null;
        try {
            c = DB.getInstance();
            String request = "select * from voituremarque ";
            preparedStatement = c.prepareStatement(request);
            resultSet = preparedStatement.executeQuery();
            Voiture voiture_tmp = null;
            while(resultSet.next()){
                voiture_tmp = new Voiture(
                        resultSet.getString("id"), 
                        resultSet.getString("matricule"), 
                        resultSet.getString("idmarque")
                );
                v.add(voiture_tmp);
            }
            int size = v.size();
            voitures = new Voiture[size];
            for(int i = 0 ; i < v.size() ; i++){
                voitures[i] = (Voiture)v.elementAt(i);
            }
        } catch (Exception ex) {
            throw ex;
        }finally{
            if(resultSet != null )
                resultSet.close();
            if(preparedStatement != null )
                preparedStatement.close();
            if(c != null )
                c.close();
        }
        
        return voitures;
    } 
}
