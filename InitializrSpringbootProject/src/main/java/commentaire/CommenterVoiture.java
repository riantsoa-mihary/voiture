/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commentaire;

import java.sql.Connection;
import simpledao.DB;
import utilisateur.Token;
import utilisateur.Utilisateur;
import voiture.Voiture;

/**
 *
 * @author riantsoa
 */
public class CommenterVoiture {
    private Token token ;
    private Voiture voiture;
    private Commentaire commentaire;
    public CommenterVoiture(String Token, String idVoiture, String commentaire) throws Exception{
        this.voiture = new Voiture(idVoiture);
        this.commentaire = new Commentaire(commentaire);
        this.commentaire.setIdVoiture(idVoiture);
        this.token = new Token(Token);
    }
    public void doIt() throws Exception{
        Connection c = null;
        try {
            c = DB.getInstance();
            Utilisateur u = this.token.getUtilisateur(c);
            this.commentaire.setIdUtilisateur(u.getId());
            this.commentaire.insert(c);
        } catch (Exception ex) {
            throw ex;
        }finally{
            if(c != null)
                c.close();
        }        
    }
    public static void doIt(String Token, String idVoiture, String commentaire) throws Exception{
        CommenterVoiture cv = new CommenterVoiture(Token, idVoiture, commentaire);
        cv.doIt();
    }
}
