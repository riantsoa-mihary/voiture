/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commentaire;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import simpledao.DB;
import voiture.Voiture;

/**
 *
 * @author riantsoa
 */
public class CommentaireDetail extends Commentaire{
    public String id;
    public CommentaireDetail(String id, String idUtilisateur, String idVoiture, String commentaire) throws Exception {
        super(idVoiture, commentaire);
        this.setId(id);
        this.setIdUtilisateur(idUtilisateur);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public static CommentaireDetail[] listeCommentaire(Connection c, String idVoiture) throws Exception{
        CommentaireDetail[] liste = null;
        Vector v = new Vector();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet =  null;
        try {
            c = DB.getInstance();
            String request = "select * from commentaire where idvoiture like ? ";
            preparedStatement = c.prepareStatement(request);
            preparedStatement.setString(1, idVoiture);
            resultSet = preparedStatement.executeQuery();
            CommentaireDetail tmp = null;
            while(resultSet.next()){
                tmp = new CommentaireDetail(
                        resultSet.getString("id"), 
                        resultSet.getString("idutilisateur"), 
                        resultSet.getString("idvoiture"), 
                        resultSet.getString("commentaire")
                );
                v.add(tmp);
            }
            int size = v.size();
            liste = new CommentaireDetail[size];
            for(int i = 0 ; i < v.size() ; i++){
                liste[i] = (CommentaireDetail)v.elementAt(i);
            }
        } catch (Exception ex) {
            throw ex;
        }finally{
            if(resultSet != null )
                resultSet.close();
            if(preparedStatement != null )
                preparedStatement.close();
        }
        return liste;
    }
    
}
