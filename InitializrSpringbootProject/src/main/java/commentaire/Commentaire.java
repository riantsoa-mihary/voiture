/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commentaire;

import java.sql.Connection;
import simpledao.SimpleDAO;

/**
 *
 * @author riantsoa
 */
public class Commentaire {
    private String idUtilisateur, idVoiture;
    private String commentaire;

    public Commentaire(String commentaire) throws Exception{
        this.setCommentaire(commentaire);
    }
    public Commentaire(String idVoiture, String commentaire) throws Exception {
        this.setIdVoiture(idVoiture);
        this.setCommentaire(commentaire); 
    }
    public void insert(Connection c) throws Exception{
        String request = " INSERT INTO commentaire ";
        request += "VALUES('COM'||nextval('seq_commentaire'), ";
        request += "'"+this.getIdUtilisateur()+"',";
        request += "'"+this.getIdVoiture()+"',";
        request += "'"+this.getCommentaire()+"')";
        SimpleDAO.create(request, c);
    }

    
    public String getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(String idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getIdVoiture() {
        return idVoiture;
    }

    public void setIdVoiture(String idVoiture) throws Exception {
        if(idVoiture == null || idVoiture.equalsIgnoreCase(""))
            throw new Exception("Voiture manquante");
        this.idVoiture = idVoiture;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) throws Exception {
        if(commentaire == null || commentaire.equalsIgnoreCase(""))
            throw new Exception("commentaire manquant");
        this.commentaire = commentaire;
    }
    
}
